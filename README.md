This is a simple crawler web application

to use the program you can do the following:

- download project and then run:  Composer update or composer install
- create new database and make its configration in .env file.
- run php artisan migrate to add tables to database.
- run php artisan serve to start the project.

after you run the server enter a valid URL then submit to save all its links and all links of first (50) sub pages.


notes:

- it takes some long time to get all links of the web page and its sub Pages, to avoid (maximum execution time of 60 seconds exceeded)
  you can reduce number of sub pages to (20) or increase 'max_execution_time' in php.ini.
- you will find a crawler.sql file in db folder that have output links from this url (https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste)
  
thanks for your attention ,,,
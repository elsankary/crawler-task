-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2018 at 01:23 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crawler`
--

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(10) UNSIGNED NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `link`, `created_at`, `updated_at`) VALUES
(161, 'https://www.homegate.ch/de', '2018-09-08 08:54:26', '2018-09-08 08:54:26'),
(162, 'https://www.homegate.ch/de?0-1.ILinkListener-header-login1', '2018-09-08 08:54:27', '2018-09-08 08:54:27'),
(163, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:54:27', '2018-09-08 08:54:27'),
(164, 'https://www.homegate.ch/inserieren/inserat-online-erfassen', '2018-09-08 08:54:29', '2018-09-08 08:54:29'),
(165, 'https://www.homegate.ch/mieten/immobilie-suchen', '2018-09-08 08:54:30', '2018-09-08 08:54:30'),
(166, 'https://www.homegate.ch/mieten/immobilie-suchen?0-1.ILinkListener-header-login1', '2018-09-08 08:54:31', '2018-09-08 08:54:31'),
(167, 'https://www.homegate.ch/kaufen/immobilie-suchen', '2018-09-08 08:54:31', '2018-09-08 08:54:31'),
(168, 'https://www.homegate.ch/kaufen/immobilie-suchen?0-1.ILinkListener-header-login1', '2018-09-08 08:54:31', '2018-09-08 08:54:31'),
(169, 'https://www.homegate.ch/hypotheken', '2018-09-08 08:54:31', '2018-09-08 08:54:31'),
(170, 'https://www.homegate.ch/ratgeber/', '2018-09-08 08:54:33', '2018-09-08 08:54:33'),
(171, 'https://www.homegate.ch/umziehen', '2018-09-08 08:54:33', '2018-09-08 08:54:33'),
(172, 'https://www.homegate.ch/umziehen?0-1.ILinkListener-header-login1', '2018-09-08 08:54:34', '2018-09-08 08:54:34'),
(173, 'https://www.homegate.ch/louer/biens-immobiliers/canton-zurich/liste-annonces', '2018-09-08 08:54:34', '2018-09-08 08:54:34'),
(174, 'https://www.homegate.ch/fr', '2018-09-08 08:54:35', '2018-09-08 08:54:35'),
(175, 'https://www.homegate.ch/affittare/immobile/cantone-zurigo/lista-annunci', '2018-09-08 08:54:35', '2018-09-08 08:54:35'),
(176, 'https://www.homegate.ch/it', '2018-09-08 08:54:37', '2018-09-08 08:54:37'),
(177, 'https://www.homegate.ch/rent/real-estate/canton-zurich/matching-list', '2018-09-08 08:54:37', '2018-09-08 08:54:37'),
(178, 'https://www.homegate.ch/en', '2018-09-08 08:54:38', '2018-09-08 08:54:38'),
(179, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login2', '2018-09-08 08:54:38', '2018-09-08 08:54:38'),
(180, 'https://www.homegate.ch/ueber-uns/rechtliches/agb', '2018-09-08 08:54:41', '2018-09-08 08:54:41'),
(181, 'https://www.homegate.ch/ueber-uns/rechtliches/privacy-policy', '2018-09-08 08:54:41', '2018-09-08 08:54:41'),
(182, 'https://www.homegate.ch/ueber-uns/rechtliches/privacy-policy?0-1.ILinkListener-header-login1', '2018-09-08 08:54:42', '2018-09-08 08:54:42'),
(183, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-1-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:54:42', '2018-09-08 08:54:42'),
(184, 'https://www.homegate.ch/mieten/108824868', '2018-09-08 08:54:45', '2018-09-08 08:54:45'),
(185, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-2-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:54:46', '2018-09-08 08:54:46'),
(186, 'https://www.homegate.ch/mieten/107790705', '2018-09-08 08:54:48', '2018-09-08 08:54:48'),
(187, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-3-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:54:48', '2018-09-08 08:54:48'),
(188, 'https://www.homegate.ch/mieten/107790702', '2018-09-08 08:54:50', '2018-09-08 08:54:50'),
(189, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-4-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:54:50', '2018-09-08 08:54:50'),
(190, 'https://www.homegate.ch/mieten/107790701', '2018-09-08 08:54:53', '2018-09-08 08:54:53'),
(191, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-5-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:54:53', '2018-09-08 08:54:53'),
(192, 'https://www.homegate.ch/mieten/108949054', '2018-09-08 08:54:59', '2018-09-08 08:54:59'),
(193, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-6-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:54:59', '2018-09-08 08:54:59'),
(194, 'https://www.homegate.ch/mieten/108424918', '2018-09-08 08:55:01', '2018-09-08 08:55:01'),
(195, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-7-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:01', '2018-09-08 08:55:01'),
(196, 'https://www.homegate.ch/mieten/108941588', '2018-09-08 08:55:03', '2018-09-08 08:55:03'),
(197, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-8-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:03', '2018-09-08 08:55:03'),
(198, 'https://www.homegate.ch/mieten/108606017', '2018-09-08 08:55:06', '2018-09-08 08:55:06'),
(199, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-9-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:06', '2018-09-08 08:55:06'),
(200, 'https://www.homegate.ch/mieten/108697341', '2018-09-08 08:55:09', '2018-09-08 08:55:09'),
(201, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-10-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:09', '2018-09-08 08:55:09'),
(202, 'https://www.homegate.ch/mieten/108941592', '2018-09-08 08:55:12', '2018-09-08 08:55:12'),
(203, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-11-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:12', '2018-09-08 08:55:12'),
(204, 'https://www.homegate.ch/mieten/108934243', '2018-09-08 08:55:14', '2018-09-08 08:55:14'),
(205, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-12-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:14', '2018-09-08 08:55:14'),
(206, 'https://www.homegate.ch/mieten/108934244', '2018-09-08 08:55:17', '2018-09-08 08:55:17'),
(207, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-13-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:17', '2018-09-08 08:55:17'),
(208, 'https://www.homegate.ch/mieten/108629086', '2018-09-08 08:55:19', '2018-09-08 08:55:19'),
(209, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-14-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:19', '2018-09-08 08:55:19'),
(210, 'https://www.homegate.ch/mieten/108625136', '2018-09-08 08:55:21', '2018-09-08 08:55:21'),
(211, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-15-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:21', '2018-09-08 08:55:21'),
(212, 'https://www.homegate.ch/mieten/108616977', '2018-09-08 08:55:23', '2018-09-08 08:55:23'),
(213, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-16-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:23', '2018-09-08 08:55:23'),
(214, 'https://www.homegate.ch/mieten/108911485', '2018-09-08 08:55:25', '2018-09-08 08:55:25'),
(215, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-17-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:25', '2018-09-08 08:55:25'),
(216, 'https://www.homegate.ch/mieten/108724339', '2018-09-08 08:55:28', '2018-09-08 08:55:28'),
(217, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-18-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:28', '2018-09-08 08:55:28'),
(218, 'https://www.homegate.ch/mieten/108720963', '2018-09-08 08:55:30', '2018-09-08 08:55:30'),
(219, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-19-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:30', '2018-09-08 08:55:30'),
(220, 'https://www.homegate.ch/mieten/108873030', '2018-09-08 08:55:32', '2018-09-08 08:55:32'),
(221, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-resultItems-20-resultItemPanel-favorite-unauthorizedLink', '2018-09-08 08:55:33', '2018-09-08 08:55:33'),
(222, 'https://www.homegate.ch/mieten/108680984', '2018-09-08 08:55:35', '2018-09-08 08:55:35'),
(223, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?ep=1', '2018-09-08 08:55:35', '2018-09-08 08:55:35'),
(224, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1&ep=1', '2018-09-08 08:55:36', '2018-09-08 08:55:36'),
(225, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?ep=2', '2018-09-08 08:55:36', '2018-09-08 08:55:36'),
(226, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1&ep=2', '2018-09-08 08:55:37', '2018-09-08 08:55:37'),
(227, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?ep=3', '2018-09-08 08:55:38', '2018-09-08 08:55:38'),
(228, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1&ep=3', '2018-09-08 08:55:40', '2018-09-08 08:55:40'),
(229, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?ep=4', '2018-09-08 08:55:40', '2018-09-08 08:55:40'),
(230, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1&ep=4', '2018-09-08 08:55:41', '2018-09-08 08:55:41'),
(231, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?ep=5', '2018-09-08 08:55:41', '2018-09-08 08:55:41'),
(232, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1&ep=5', '2018-09-08 08:55:43', '2018-09-08 08:55:43'),
(233, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?ep=6', '2018-09-08 08:55:43', '2018-09-08 08:55:43'),
(234, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1&ep=6', '2018-09-08 08:55:44', '2018-09-08 08:55:44'),
(235, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?ep=7', '2018-09-08 08:55:44', '2018-09-08 08:55:44'),
(236, 'https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1&ep=7', '2018-09-08 08:55:45', '2018-09-08 08:55:45'),
(237, 'https://www.homegate.ch/mieten/wohnung/kanton-zuerich/trefferliste', '2018-09-08 08:55:45', '2018-09-08 08:55:45'),
(238, 'https://www.homegate.ch/mieten/wohnung/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:46', '2018-09-08 08:55:46'),
(239, 'https://www.homegate.ch/mieten/haus/kanton-zuerich/trefferliste', '2018-09-08 08:55:46', '2018-09-08 08:55:46'),
(240, 'https://www.homegate.ch/mieten/haus/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:47', '2018-09-08 08:55:47'),
(241, 'https://www.homegate.ch/mieten/moebliertes-wohnobjekt/kanton-zuerich/trefferliste', '2018-09-08 08:55:47', '2018-09-08 08:55:47'),
(242, 'https://www.homegate.ch/mieten/moebliertes-wohnobjekt/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:48', '2018-09-08 08:55:48'),
(243, 'https://www.homegate.ch/mieten/parkplatz-garage/kanton-zuerich/trefferliste', '2018-09-08 08:55:48', '2018-09-08 08:55:48'),
(244, 'https://www.homegate.ch/mieten/parkplatz-garage/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:50', '2018-09-08 08:55:50'),
(245, 'https://www.homegate.ch/mieten/buero/kanton-zuerich/trefferliste', '2018-09-08 08:55:50', '2018-09-08 08:55:50'),
(246, 'https://www.homegate.ch/mieten/buero/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:51', '2018-09-08 08:55:51'),
(247, 'https://www.homegate.ch/mieten/gewerbeobjekt/kanton-zuerich/trefferliste', '2018-09-08 08:55:51', '2018-09-08 08:55:51'),
(248, 'https://www.homegate.ch/mieten/gewerbeobjekt/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:52', '2018-09-08 08:55:52'),
(249, 'https://www.homegate.ch/mieten/lager/kanton-zuerich/trefferliste', '2018-09-08 08:55:52', '2018-09-08 08:55:52'),
(250, 'https://www.homegate.ch/mieten/lager/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:53', '2018-09-08 08:55:53'),
(251, 'https://www.homegate.ch/kaufen/wohnung/kanton-zuerich/trefferliste', '2018-09-08 08:55:53', '2018-09-08 08:55:53'),
(252, 'https://www.homegate.ch/kaufen/wohnung/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:55', '2018-09-08 08:55:55'),
(253, 'https://www.homegate.ch/kaufen/haus/kanton-zuerich/trefferliste', '2018-09-08 08:55:55', '2018-09-08 08:55:55'),
(254, 'https://www.homegate.ch/kaufen/haus/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:56', '2018-09-08 08:55:56'),
(255, 'https://www.homegate.ch/kaufen/immobilien/kanton-zuerich/trefferliste', '2018-09-08 08:55:56', '2018-09-08 08:55:56'),
(256, 'https://www.homegate.ch/kaufen/immobilien/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:55:58', '2018-09-08 08:55:58'),
(257, 'https://www.homegate.ch/kaufen/bauland/kanton-zuerich/trefferliste', '2018-09-08 08:55:58', '2018-09-08 08:55:58'),
(258, 'https://www.homegate.ch/kaufen/bauland/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:56:00', '2018-09-08 08:56:00'),
(259, 'https://www.homegate.ch/kaufen/mehrfamilienhaus/kanton-zuerich/trefferliste', '2018-09-08 08:56:00', '2018-09-08 08:56:00'),
(260, 'https://www.homegate.ch/kaufen/mehrfamilienhaus/kanton-zuerich/trefferliste?0-1.ILinkListener-header-login1', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(261, 'https://www.homegate.ch/kaufen/wohn-geschaeftshaus/kanton-zuerich/trefferliste', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(262, 'https://www.homegate.ch/kaufen/parkplatz-garage/kanton-zuerich/trefferliste', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(263, 'https://www.homegate.ch/kaufen/gewerbeobjekt/kanton-zuerich/trefferliste', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(264, 'https://www.homegate.ch/ueber-uns/unternehmen/homegate', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(265, 'https://www.homegate.ch/ueber-uns/unternehmen/kontakt', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(266, 'https://www.homegate.ch/ueber-uns/unternehmen/newsletter', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(267, 'https://www.homegate.ch/ueber-uns/rechtliches/impressum', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(268, 'https://www.homegate.ch/sitemap', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(269, 'https://www.homegate.ch/inserieren/inserat-online-erfassen?tr=7', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(270, 'https://www.homegate.ch/mieten?tr=8', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(271, 'https://www.homegate.ch/kaufen?tr=9', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(272, 'https://www.homegate.ch/umziehen?tr=11', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(273, 'https://www.homegate.ch/inserieren/inserat-erfassen/inserat-online-erfassen?tr=13', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(274, 'https://www.homegate.ch/mieten/immobilie-suchen/suchabo', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(275, 'https://www.homegate.ch/handy', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(276, 'https://www.homegate.ch/mieten/bewerbungsdossier', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(277, 'https://www.homegate.ch/kaufen/wohneigentum-schaetzen', '2018-09-08 08:56:01', '2018-09-08 08:56:01'),
(278, 'https://www.homegate.ch/ueber-uns/partner/partner-werden/werbepartner', '2018-09-08 08:56:02', '2018-09-08 08:56:02'),
(279, 'https://www.homegate.ch/kaufen/schritte-zum-eigenheim', '2018-09-08 08:56:02', '2018-09-08 08:56:02'),
(280, 'https://www.homegate.ch/umziehen/umzugsplanung/ratgeber-planung/umzugsplaner', '2018-09-08 08:56:02', '2018-09-08 08:56:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_07_171451_create_links_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

namespace App\Http\Controllers;

use App\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    //set all found links to avoid links repetition
    public $found_links = array();

    //number of sub Pages called in Recursive function
    public $subPages_count = 20;

    function crawlPage(Request $request)
    {
        $url = $request->url;
        $depth = 1;
        $this->crawl_page($url,$depth);
        return view('successPage');
    }

    function crawl_page($url, $depth)
    {
        //create new DomDocument to load html file
        $webPage = new \DOMDocument();
        libxml_use_internal_errors(true);
        $webPage->loadHTMLFile($url);
        libxml_use_internal_errors(false);

        //get all <a> tags in the wep page
        $ATags = $webPage->getElementsByTagName('a');

        foreach ($ATags as $ATag) {
            $href = $ATag->getAttribute('href');

            //get valid links and remove social network links like (facebook - linkedin)
            if (0 !== strpos($href, 'http') && 0 === strpos($href, '/')) {
                $path = '/' . ltrim($href, '/');
                $parts = parse_url($url);
                $href = $parts['scheme'] . '://';
                $href .= $parts['host'];
                $href .= $path;

                //check if the link is already saved before
                if (!isset($this->found_links[$href])){
                    //save links in db
                    Link::create(['link' => $href]);
                    
                    $this->found_links[$href] = true;
                    if ($depth === 0){
                        return;
                    }
                    //call the Recursive function for the first 20 sub pages
                    else if($this->subPages_count > 0) {
                        $this->subPages_count--;
                        $this->crawl_page($href, $depth - 1);
                    }
                }
            }


        }
    }
}
